@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"> Błąd </div>

                    <div class="card-body">

                        <div class="alert alert-error" role="alert">
                            <h4 class="alert-heading"> Informacja juz istnieje  </h4>
                            <p class="mb-0">
                                <b> Identyfikator wniosku: </b>  {{ $email }} <br>
                                <br>


                    </div>



                </div>


                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
