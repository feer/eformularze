@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"> Formularz prośby o wydanie zaświadczenia dla wolontariusza </div>

                    <div class="card-body">


                        {!!Form::open()->method('get')->route('VolunteerCertificationStore' )!!}
                        {!!Form::text('name', 'Imię i nazwisko')->required(true)!!}
                        {!!Form::date('dateOfBirth', 'Data urodzenia')->required(true)!!}
                        <h5> Dane kontaktowe </h5>
                        {!!Form::text('email', 'Adres e-mail')!!}
                        {!!Form::submit("Wyślji prośbę",'success')!!}



                    </div>



                </div>


                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
