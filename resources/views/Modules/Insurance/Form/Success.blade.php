@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"> Formularz przekazania informacji do ubezpieczenia - potwierdzenie </div>

                    <div class="card-body">

                        <div class="alert alert-success" role="alert">
                            <h4 class="alert-heading"> Potwierdzenie przekazania informacji  </h4>
                            <p class="mb-0">
                                <b> Data przekazania : </b>  {{ $data }} <br>
                                <b> Identyfikator: </b>  {{ $email }} <br>
                                <b> Status: </b>  przekazana
                                <br>


                    </div>



                </div>


                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
