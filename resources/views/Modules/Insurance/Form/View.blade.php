@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"> Formularz zgłoszenia wolontairusza do ubezpieczenia </div>

                    <div class="card-body">


                        <div class="alert alert-secondary" role="alert">
                            <strong>Wniosek nie powoduje skutków prawnych </strong> <br>
                            Ten formularz służy jedynie do przesłania informacji dot. ubezpieczenia.
                            <ul>
                                <li> Nie wymaga podania numeru PESEL.</li>
                                <li> Zgłoszenie do ubezpieczenaia nastąpi po weryfikacji.</li>
                            </ul>

                       </div>
                        {!!Form::open()->method('get')->route('InsuranceFormStore' )!!}
                        {!!Form::text('name', 'Imię i nazwisko')->required(true)!!}
                        {!!Form::date('dateOfBirth', 'Data urodzenia')->required(true)!!}
                        <h5> Dane kontaktowe </h5>
                        {!!Form::text('email', 'Adres e-mail')!!}
                        {!!Form::text('phoneNumber', 'Numer telefonu')!!}
                        <h5> Informacje do ubezpieczenia </h5>
                        {!!Form::select('disabillityStatus', 'Czy jesteś osobą niepełnosprawną?', ['tak' => 'Tak', 'Nie' => 'Nie'])!!}
                        {!!Form::select('chronicDisease', 'Czy jesteś osobą z chorobami przewlekłymi?', ['tak' => 'Tak', 'Nie' => 'Nie'])!!}
                        <h5> Informacje dotyczące wniosku  </h5>
                        {!!Form::checkbox('blind', 'Osoba niewidoma')!!}
                        <small class="text-muted">Zaznaczenie tego pola ma wpływ na sposób podpisywania dokumentów wniosku. </small> <br> <br>
                        {!!Form::submit("Przekaż informację",'success')!!}



                    </div>



                </div>


                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
