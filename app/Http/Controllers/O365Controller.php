<?php

namespace App\Http\Controllers;

use App\o365;
use Illuminate\Http\Request;

class O365Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\o365  $o365
     * @return \Illuminate\Http\Response
     */
    public function show(o365 $o365)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\o365  $o365
     * @return \Illuminate\Http\Response
     */
    public function edit(o365 $o365)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\o365  $o365
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, o365 $o365)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\o365  $o365
     * @return \Illuminate\Http\Response
     */
    public function destroy(o365 $o365)
    {
        //
    }
}
