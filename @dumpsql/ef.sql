-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 13, 2020 at 07:58 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ef`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Category 1', 'category-1', '2020-06-11 18:27:10', '2020-06-11 18:27:10'),
(2, NULL, 1, 'Category 2', 'category-2', '2020-06-11 18:27:10', '2020-06-11 18:27:10');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(23, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(24, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(25, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 4),
(26, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(27, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 6),
(28, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(29, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(30, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, NULL, 2),
(31, 5, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, NULL, 3),
(32, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 4),
(33, 5, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 5),
(34, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 6),
(35, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(36, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(37, 5, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 9),
(38, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 10),
(39, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(40, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 12),
(41, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 13),
(42, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, NULL, 14),
(43, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, NULL, 15),
(44, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(45, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, NULL, 2),
(46, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 3),
(47, 6, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 4),
(48, 6, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 5),
(49, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 6),
(50, 6, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 7),
(51, 6, 'meta_keywords', 'text', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 8),
(52, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(53, 6, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, NULL, 10),
(54, 6, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, NULL, 11),
(55, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, NULL, 12),
(56, 7, 'id', 'text', 'Id wniosku', 1, 0, 0, 0, 0, 0, '{}', 1),
(57, 7, 'name', 'text', 'Imię i nazwisko', 1, 1, 1, 1, 1, 1, '{}', 2),
(58, 7, 'dateOfBirth', 'text', 'Data urodzenia', 1, 1, 1, 1, 1, 1, '{}', 3),
(59, 7, 'email', 'text', 'Adres e-mail', 1, 1, 1, 1, 1, 1, '{}', 4),
(60, 7, 'disabillityStatus', 'checkbox', 'Osoba niepełnosprawna', 1, 1, 1, 1, 1, 1, '{}', 5),
(61, 7, 'chronicDisease', 'checkbox', 'Choroby przewlekłe', 1, 1, 1, 1, 1, 1, '{}', 6),
(62, 7, 'applicationDate', 'date', 'Data sprawy w EZD', 1, 1, 1, 1, 1, 1, '{}', 7),
(63, 7, 'applicationStatus', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{}', 8),
(64, 7, 'applicationNumber', 'text', 'Numer sprawy w EZD', 1, 1, 1, 1, 1, 1, '{}', 10),
(65, 7, 'uploadStatus', 'checkbox', 'Dokument zatwierdzony`', 1, 1, 1, 1, 1, 1, '{}', 11),
(66, 7, 'uploadPath', 'file', 'Wzór', 1, 1, 1, 1, 1, 1, '{}', 12),
(67, 7, 'documentPath', 'text', 'Skan dokumentu', 1, 1, 1, 1, 1, 1, '{}', 13),
(68, 7, 'vendorStatus', 'text', 'Status zgłoszenia', 1, 1, 1, 1, 1, 1, '{}', 14),
(69, 7, 'operatorName', 'text', 'Nazwa operatora', 1, 1, 1, 1, 1, 1, '{}', 15),
(70, 7, 'vendorNumber', 'text', 'Numer operatora', 1, 1, 1, 1, 1, 1, '{}', 16),
(71, 7, 'phoneNumber', 'text', 'Numer telefonu', 1, 1, 1, 1, 1, 1, '{}', 17),
(72, 7, 'blind', 'checkbox', 'Osoba niewidoma', 1, 1, 1, 1, 1, 1, '{}', 9),
(73, 7, 'created_at', 'timestamp', 'Data utworzenia', 0, 1, 1, 1, 0, 1, '{}', 18),
(74, 7, 'updated_at', 'timestamp', 'Data aktualizacji', 0, 0, 0, 0, 0, 0, '{}', 19),
(75, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(76, 8, 'name', 'text', 'Imię i nazwisko', 1, 1, 1, 1, 1, 1, '{}', 2),
(77, 8, 'dateOfBirth', 'text', 'Data urodzenia', 1, 1, 1, 1, 1, 1, '{}', 3),
(78, 8, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 4),
(79, 8, 'goal', 'text', 'Cel', 1, 1, 1, 1, 1, 1, '{}', 5),
(80, 8, 'uzasadnienie', 'text', 'Uzasadnienie', 1, 1, 1, 1, 1, 1, '{}', 6),
(81, 8, 'gpdr', 'text', 'Zgoda RODO', 1, 1, 1, 1, 1, 1, '{}', 7),
(82, 8, 'created_at', 'timestamp', 'Data utworzenia', 0, 1, 1, 1, 0, 1, '{}', 8),
(83, 8, 'updated_at', 'timestamp', 'Data aktualizacji', 0, 0, 0, 0, 0, 0, '{}', 9);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2020-06-11 18:27:10', '2020-06-11 18:27:10'),
(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, NULL, '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(7, 'insurances', 'insurances', 'Ubezpieczenie', 'Insurances', 'voyager-move', 'App\\Insurance', NULL, NULL, NULL, 1, 0, '{\"order_column\":\"name\",\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-06-11 18:35:06', '2020-06-11 18:58:19'),
(8, 'volunteer_certifications', 'volunteer-certifications', 'Prośby o zaśiwadczenia', 'Prośby o zaśiwadczenia', NULL, 'App\\VolunteerCertification', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-06-11 19:14:34', '2020-06-11 19:14:34');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `insurances`
--

CREATE TABLE `insurances` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateOfBirth` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `disabillityStatus` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `chronicDisease` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `applicationDate` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `applicationStatus` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `applicationNumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uploadStatus` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uploadPath` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `documentPath` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vendorStatus` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `operatorName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vendorNumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phoneNumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `blind` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `insurances`
--

INSERT INTO `insurances` (`id`, `name`, `dateOfBirth`, `email`, `disabillityStatus`, `chronicDisease`, `applicationDate`, `applicationStatus`, `applicationNumber`, `uploadStatus`, `uploadPath`, `documentPath`, `vendorStatus`, `operatorName`, `vendorNumber`, `phoneNumber`, `blind`, `created_at`, `updated_at`) VALUES
(69, 'fddf', '1923-12-13', 'ziemowit.gil@interia.pl', 'tak', 'Nie', '', '', '', '', '', '', '', '', '', '535231923', 'on', '2020-06-11 16:03:28', '2020-06-11 16:03:28'),
(70, 'fddf', '1923-12-13', 'ziemowit.gil@interia.pl', 'tak', 'Nie', '', '', '', '', '', '', '', '', '', '535231923', 'on', '2020-06-11 16:03:33', '2020-06-11 16:03:33'),
(71, 'Ziemowit Gil', '1926-12-11', 'ziemowit.gil@feer.org.pl', 'tak', 'tak', '', '', '', '', '', '', '', '', '', '535231923', '', '2020-06-11 16:06:58', '2020-06-11 16:06:58'),
(72, 'Ziemowit Gil', '1926-12-11', 'ziemowit.gil@feer.org.pl', 'tak', 'tak', '', '', '', '', '', '', '', '', '', '535231923', '', '2020-06-11 16:07:15', '2020-06-11 16:07:15'),
(73, 'dfdfd', '1926-12-11', 'ziemowit.gil@interia.pl', 'tak', 'tak', '', '', '', '', '', '', '', '', '', '+48535231923', '', '2020-06-11 16:18:16', '2020-06-11 16:18:16'),
(74, 'dfdfd', '1926-12-11', 'ziemowit.gil@interia.pl', 'tak', 'tak', '', '', '', '', '', '', '', '', '', '+48535231923', '', '2020-06-11 16:25:18', '2020-06-11 16:25:18'),
(75, 'dfdfd', '1926-12-11', 'ziemowit.gil@interia.pl', 'tak', 'tak', '', '', '', '', '', '', '', '', '', '+48535231923', '', '2020-06-11 16:25:39', '2020-06-11 16:25:39'),
(76, 'dfdfd', '1926-12-11', 'ziemowit.gil@interia.pl', 'tak', 'tak', '', '', '', '', '', '', '', '', '', '+48535231923', '', '2020-06-11 16:25:45', '2020-06-11 16:25:45'),
(77, '43', '0504-11-11', 'zgil@eyesoft.org.pl', 'tak', 'tak', '', '', '', '', '', '', '', '', '', '535231923', '', '2020-06-11 16:27:09', '2020-06-11 16:27:09'),
(78, '43', '0504-11-11', 'zgil@eyesoft.org.pl', 'tak', 'tak', '', '', '', '', '', '', '', '', '', '535231923', '', '2020-06-11 16:28:06', '2020-06-11 16:28:06'),
(79, 'Ziemowit Gil', '1925-12-11', 'ziemowit.gil@interia.pl', 'tak', 'tak', '', '', '', '', '', '', '', '', '', '+48535231923', '', '2020-06-11 16:29:18', '2020-06-11 16:29:18'),
(80, 'Ziemowit Gil', '1925-12-11', 'ziemowit.gil@interia.pl', 'tak', 'tak', '', '', '', '', '', '', '', '', '', '+48535231923', '', '2020-06-11 16:29:26', '2020-06-11 16:29:26'),
(81, 'Ziemowit Gil', '1925-12-11', 'ziemowit.gil@interia.pl', 'tak', 'tak', '', '', '', '', '', '', '', '', '', '+48535231923', '', '2020-06-11 16:29:35', '2020-06-11 16:29:35'),
(82, 'Ziemowit Gil', '1925-12-11', 'ziemowit.gil@interia.pl', 'tak', 'tak', '', '', '', '', '', '', '', '', '', '+48535231923', '', '2020-06-11 16:30:43', '2020-06-11 16:30:43'),
(83, 'Ziemowit Gil', '1925-12-11', 'ziemowit.gil@interia.pl', 'tak', 'tak', '', '', '', '', '', '', '', '', '', '+48535231923', '', '2020-06-11 16:33:32', '2020-06-11 16:33:32'),
(84, 'Ziemowit Gil', '1925-12-11', 'ziemowit.gil@interia.pl', 'tak', 'tak', '', '', '', '', '', '', '', '', '', '+48535231923', '', '2020-06-11 16:33:47', '2020-06-11 16:33:47'),
(85, 'Ziemowit Gil', '1925-12-11', 'ziemowit.gil@interia.pl', 'tak', 'tak', '', '', '', '', '', '', '', '', '', '+48535231923', '', '2020-06-11 16:33:53', '2020-06-11 16:33:53'),
(86, 'Ziemowit Gil', '1925-12-11', 'ziemowit.gil@interia.pl', 'tak', 'tak', '', '', '', '', '', '', '', '', '', '+48535231923', '', '2020-06-11 16:33:53', '2020-06-11 16:33:53'),
(87, 'Ziemowit Gil', '1925-12-11', 'ziemowit.gil@interia.pl', 'tak', 'tak', '', '', '', '', '', '', '', '', '', '+48535231923', '', '2020-06-11 16:34:14', '2020-06-11 16:34:14'),
(88, 'Ziemowit Gil', '1925-12-11', 'ziemowit.gil@interia.pl', 'tak', 'tak', '', '', '', '', '', '', '', '', '', '+48535231923', '', '2020-06-11 16:36:10', '2020-06-11 16:36:10'),
(89, 'Ziemowit Gil', '1925-12-11', 'ziemowit.gil@interia.pl', 'tak', 'tak', '', '', '', '', '', '', '', '', '', '+48535231923', '', '2020-06-11 16:36:15', '2020-06-11 16:36:15'),
(90, 'Ziemowit Gil', '1925-12-11', 'ziemowit.gil@interia.pl', 'tak', 'tak', '', '', '', '', '', '', '', '', '', '+48535231923', '', '2020-06-11 16:37:41', '2020-06-11 16:37:41'),
(91, 'dfdfd', '1926-12-11', 'ziemowit.gil@interia.pl', 'tak', 'tak', '', '', '', '', '', '', '', '', '', '+48535231923', '', '2020-06-11 16:39:23', '2020-06-11 16:39:23'),
(92, 'TT', '1212-12-11', 'DWE@WP.PL', 'tak', 'tak', '', '', '', '', '', '', '', '', '', '121331', '', '2020-06-11 17:40:31', '2020-06-11 17:40:31'),
(93, 'TT', '1212-12-11', 'DWE@WP.PL', 'tak', 'tak', '', '', '', '', '', '', '', '', '', '121331', '', '2020-06-11 17:41:12', '2020-06-11 17:41:12'),
(94, 'TT', '1212-12-11', 'DWE@WP.PL', 'tak', 'tak', '', '', '', '', '', '', '', '', '', '121331', '', '2020-06-11 17:41:26', '2020-06-11 17:41:26'),
(95, 'efdf', '0023-02-11', 'ziemowit.gil1997@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-06-11 18:07:57', '2020-06-11 18:07:57'),
(96, 'dffd', '1981-05-11', 'ziemowit.gil@feer.org.pl', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-06-11 18:09:47', '2020-06-11 18:09:47'),
(97, 'dffdfd', '61926-02-11', 'demos@softaculous.com', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-06-11 18:10:49', '2020-06-11 18:10:49'),
(98, 'dffdfd', '61926-02-11', 'demos@softaculous.com', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-06-11 18:11:13', '2020-06-11 18:11:13'),
(99, 'dffdfd', '61926-02-11', 'demos@softaculous.com', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-06-11 18:11:22', '2020-06-11 18:11:22'),
(100, 'dffdfd', '61926-02-11', 'demos@softaculous.com', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-06-11 18:11:41', '2020-06-11 18:11:41'),
(101, 'fddf', '2019-02-11', 'ziemowit.gil@feer.org.pl', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-06-11 18:18:09', '2020-06-11 18:18:09'),
(102, 'dssddd', '1112-12-12', 'ziemowit.gil@feer.org.pl', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-06-11 18:41:47', '2020-06-11 18:41:47'),
(103, 'TEST', '71987-08-11', 'ziemowit.gil@feer.org.pl', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-06-11 18:52:31', '2020-06-11 18:52:31'),
(104, 'TEST', '71987-08-11', 'ziemowit.gil@feer.org.pl', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-06-11 18:53:28', '2020-06-11 18:53:28'),
(105, 'TEST', '71987-08-11', 'ziemowit.gil@feer.org.pl', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-06-11 18:53:32', '2020-06-11 18:53:32');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-06-11 18:27:09', '2020-06-11 18:27:09');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2020-06-11 18:27:09', '2020-06-11 18:27:09', 'voyager.dashboard', NULL),
(3, 1, 'Konta', '', '_self', 'voyager-person', '#ffea00', 16, 2, '2020-06-11 18:27:09', '2020-06-11 18:43:43', 'voyager.users.index', 'null'),
(4, 1, 'Role', '', '_self', 'voyager-lock', '#000000', 16, 1, '2020-06-11 18:27:09', '2020-06-11 18:43:27', 'voyager.roles.index', 'null'),
(5, 1, 'Narzędzia', '', '_self', 'voyager-tools', '#000000', NULL, 4, '2020-06-11 18:27:09', '2020-06-11 19:14:58', NULL, ''),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2020-06-11 18:27:09', '2020-06-11 18:36:38', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2020-06-11 18:27:09', '2020-06-11 18:36:38', 'voyager.database.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2020-06-11 18:27:09', '2020-06-11 18:36:38', 'voyager.bread.index', NULL),
(10, 1, 'Ustawienia', '', '_self', 'voyager-settings', '#000000', NULL, 5, '2020-06-11 18:27:09', '2020-06-11 19:14:58', 'voyager.settings.index', 'null'),
(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2020-06-11 18:27:11', '2020-06-11 18:36:38', 'voyager.hooks', NULL),
(15, 1, 'Zgłoszenia do ubezpieczenia', '', '_self', 'voyager-move', '#000000', 17, 1, '2020-06-11 18:35:06', '2020-06-11 18:57:13', 'voyager.insurances.index', 'null'),
(16, 1, 'Użytkownicy', '', '_self', NULL, '#000000', NULL, 3, '2020-06-11 18:36:27', '2020-06-11 19:14:58', NULL, ''),
(17, 1, 'Wnioski', '#', '_self', 'voyager-move', '#000000', NULL, 2, '2020-06-11 18:57:03', '2020-06-11 18:57:12', NULL, ''),
(18, 1, 'Prośby o zaśiwadczenia', '', '_self', NULL, NULL, 17, 2, '2020-06-11 19:14:34', '2020-06-11 21:16:57', 'voyager.volunteer-certifications.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(8, '2014_10_12_000000_create_users_table', 1),
(9, '2014_10_12_100000_create_password_resets_table', 1),
(10, '2019_08_19_000000_create_failed_jobs_table', 1),
(12, '2020_06_10_191839_create_insurances_table', 2),
(13, '2020_06_11_194426_create_volunteer_certifications_table', 3),
(14, '2016_01_01_000000_add_voyager_user_fields', 4),
(15, '2016_01_01_000000_create_data_types_table', 4),
(16, '2016_05_19_173453_create_menu_table', 4),
(17, '2016_10_21_190000_create_roles_table', 4),
(18, '2016_10_21_190000_create_settings_table', 4),
(19, '2016_11_30_135954_create_permission_table', 4),
(20, '2016_11_30_141208_create_permission_role_table', 4),
(21, '2016_12_26_201236_data_types__add__server_side', 4),
(22, '2017_01_13_000000_add_route_to_menu_items_table', 4),
(23, '2017_01_14_005015_create_translations_table', 4),
(24, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 4),
(25, '2017_03_06_000000_add_controller_to_data_types_table', 4),
(26, '2017_04_21_000000_add_order_to_data_rows_table', 4),
(27, '2017_07_05_210000_add_policyname_to_data_types_table', 4),
(28, '2017_08_05_000000_add_group_to_settings_table', 4),
(29, '2017_11_26_013050_add_user_role_relationship', 4),
(30, '2017_11_26_015000_create_user_roles_table', 4),
(31, '2018_03_11_000000_add_user_settings', 4),
(32, '2018_03_14_000000_add_details_to_data_types_table', 4),
(33, '2018_03_16_000000_make_settings_value_nullable', 4),
(34, '2016_01_01_000000_create_pages_table', 5),
(35, '2016_01_01_000000_create_posts_table', 5),
(36, '2016_02_15_204651_create_categories_table', 5),
(37, '2017_04_11_000000_alter_post_nullable_fields_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2020-06-11 18:27:11', '2020-06-11 18:27:11');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(2, 'browse_bread', NULL, '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(3, 'browse_database', NULL, '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(4, 'browse_media', NULL, '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(5, 'browse_compass', NULL, '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(6, 'browse_menus', 'menus', '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(7, 'read_menus', 'menus', '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(8, 'edit_menus', 'menus', '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(9, 'add_menus', 'menus', '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(10, 'delete_menus', 'menus', '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(11, 'browse_roles', 'roles', '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(12, 'read_roles', 'roles', '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(13, 'edit_roles', 'roles', '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(14, 'add_roles', 'roles', '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(15, 'delete_roles', 'roles', '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(16, 'browse_users', 'users', '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(17, 'read_users', 'users', '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(18, 'edit_users', 'users', '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(19, 'add_users', 'users', '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(20, 'delete_users', 'users', '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(21, 'browse_settings', 'settings', '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(22, 'read_settings', 'settings', '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(23, 'edit_settings', 'settings', '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(24, 'add_settings', 'settings', '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(25, 'delete_settings', 'settings', '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(26, 'browse_categories', 'categories', '2020-06-11 18:27:10', '2020-06-11 18:27:10'),
(27, 'read_categories', 'categories', '2020-06-11 18:27:10', '2020-06-11 18:27:10'),
(28, 'edit_categories', 'categories', '2020-06-11 18:27:10', '2020-06-11 18:27:10'),
(29, 'add_categories', 'categories', '2020-06-11 18:27:10', '2020-06-11 18:27:10'),
(30, 'delete_categories', 'categories', '2020-06-11 18:27:10', '2020-06-11 18:27:10'),
(31, 'browse_posts', 'posts', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(32, 'read_posts', 'posts', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(33, 'edit_posts', 'posts', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(34, 'add_posts', 'posts', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(35, 'delete_posts', 'posts', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(36, 'browse_pages', 'pages', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(37, 'read_pages', 'pages', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(38, 'edit_pages', 'pages', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(39, 'add_pages', 'pages', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(40, 'delete_pages', 'pages', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(41, 'browse_hooks', NULL, '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(42, 'browse_insurances', 'insurances', '2020-06-11 18:35:06', '2020-06-11 18:35:06'),
(43, 'read_insurances', 'insurances', '2020-06-11 18:35:06', '2020-06-11 18:35:06'),
(44, 'edit_insurances', 'insurances', '2020-06-11 18:35:06', '2020-06-11 18:35:06'),
(45, 'add_insurances', 'insurances', '2020-06-11 18:35:06', '2020-06-11 18:35:06'),
(46, 'delete_insurances', 'insurances', '2020-06-11 18:35:06', '2020-06-11 18:35:06'),
(47, 'browse_volunteer_certifications', 'volunteer_certifications', '2020-06-11 19:14:34', '2020-06-11 19:14:34'),
(48, 'read_volunteer_certifications', 'volunteer_certifications', '2020-06-11 19:14:34', '2020-06-11 19:14:34'),
(49, 'edit_volunteer_certifications', 'volunteer_certifications', '2020-06-11 19:14:34', '2020-06-11 19:14:34'),
(50, 'add_volunteer_certifications', 'volunteer_certifications', '2020-06-11 19:14:34', '2020-06-11 19:14:34'),
(51, 'delete_volunteer_certifications', 'volunteer_certifications', '2020-06-11 19:14:34', '2020-06-11 19:14:34');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 2),
(3, 1),
(3, 2),
(4, 1),
(4, 2),
(5, 1),
(5, 2),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 2),
(42, 1),
(42, 2),
(43, 1),
(43, 2),
(44, 1),
(44, 2),
(45, 1),
(45, 2),
(46, 1),
(46, 2),
(47, 1),
(47, 2),
(48, 1),
(48, 2),
(49, 1),
(49, 2),
(50, 1),
(50, 2),
(51, 1),
(51, 2);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(1, 0, NULL, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(2, 0, NULL, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(3, 0, NULL, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(4, 0, NULL, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-06-11 18:27:11', '2020-06-11 18:27:11');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2020-06-11 18:27:09', '2020-06-11 18:27:09'),
(2, 'Operator', 'Operator', '2020-06-11 18:27:09', '2020-06-11 18:45:07');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Panel Administracyjny', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(15, 'personalizacja.NGOName', 'Nazwa podmiotu', 'Fundacja FEER', NULL, 'text', 8, 'Personalizacja'),
(16, 'admin.version', 'Wersja', '1.0.2dev120620', NULL, 'text', 9, 'Admin'),
(19, 'admin.WorkMode', 'Tryb pracy', 'dev', NULL, 'text', 10, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2020-06-11 18:27:11', '2020-06-11 18:27:11'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2020-06-11 18:27:11', '2020-06-11 18:27:11');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$G1h1SBLTsKIS9qg0YEq35uNAh2S1txHBYJYDT6Y1wp4VR2Dv5NgMC', 'e8lb1f2maY5m9FMn3ddbUEf1oaEzvzvCUUjoSxcQlUOY0KUZ960RYuW9nbtr', '{\"locale\":\"pl\"}', '2020-06-11 18:27:11', '2020-06-11 18:48:13'),
(2, 2, 'ZIemowit', 'ziemowit.gil@feer.org.pl', 'users/default.png', NULL, '$2y$10$2mFcyVGL8nlivLvKKV2OceJQ/.MRXf9SGS.kCRsjBrqCgLj7q/Zl.', NULL, '{\"locale\":\"pl\"}', '2020-06-11 18:45:38', '2020-06-11 21:39:17'),
(3, NULL, 'Ziemowit Gil2', 'ziemowit.gil1997@gmail.com', 'users/default.png', NULL, '$2y$10$/7Sz3.TNqPdrZ7S/7eLSJ.gNloJnFAzxJd/wBH6QzMbDg.VhdxPzG', NULL, NULL, '2020-06-11 22:00:07', '2020-06-11 22:00:07');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `volunteer_certifications`
--

CREATE TABLE `volunteer_certifications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateOfBirth` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `goal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uzasadnienie` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gpdr` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurances`
--
ALTER TABLE `insurances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- Indexes for table `volunteer_certifications`
--
ALTER TABLE `volunteer_certifications`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `insurances`
--
ALTER TABLE `insurances`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `volunteer_certifications`
--
ALTER TABLE `volunteer_certifications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
