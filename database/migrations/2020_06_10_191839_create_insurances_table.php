<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsurancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurances', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('dateOfBirth');
            $table->string('email');
            $table->string('disabillityStatus');
            $table->string('chronicDisease');
            $table->string('applicationDate');
            $table->string('applicationStatus');
            $table->string('applicationNumber');
            $table->string('uploadStatus');
            $table->string('uploadPath');
            $table->string('documentPath');
            $table->string('vendorStatus');
            $table->string('operatorName');
            $table->string('vendorNumber');
            $table->string('phoneNumber');
            $table->string('blind');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurances');
    }
}
