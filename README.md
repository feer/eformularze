System do eFormularzy
Zbudowany na Laravel 6

## composer.json 
        "php": "^7.2.5",
        "fideloper/proxy": "^4.2",
        "fruitcake/laravel-cors": "^1.0",
        "guzzlehttp/guzzle": "^6.3",
        "laravel/framework": "^7.0",
        "laravel/tinker": "^2.0",
        "laravel/ui": "^2.0",
        "laravelcollective/html": "^6.1",
        "mckenziearts/laravel-notify": "^1.1",
        "nesbot/carbon": "^2.35",
        "netojose/laravel-bootstrap-4-forms": "^3.0",
        "realrashid/sweet-alert": "^3.1",
        "tcg/voyager": "^1.4"